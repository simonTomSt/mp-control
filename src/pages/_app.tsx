import type { AppProps } from 'next/app';
import { useEffect } from 'react';
import { CssBaseline, StylesProvider } from '@material-ui/core';
import { ThemeProvider } from 'styled-components';
import theme from '../styles';

function MyApp({ Component, pageProps }: AppProps) {
  useEffect(() => {
    const jssStyles = document.querySelector('#jss-server-side');

    if (jssStyles) {
      // eslint-disable-next-line no-unused-expressions
      jssStyles.parentElement?.removeChild(jssStyles);
    }
  }, []);

  return (
    <StylesProvider injectFirst>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <Component {...pageProps} />
      </ThemeProvider>
    </StylesProvider>
  );
}
export default MyApp;
